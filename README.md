# Eternaltwin target state configuration

This repository hosts the configuration for the wanted state of the Eternaltwin servers.

Permissions are controlled using the `CODEOWNERS` file.

## Deployment

1. Update the `release` commit for your application. It is located in `./pousty/apps/<appName>/channels/<channelName>.json`.
2. Submit your change through a new GitLab Merge Request.
3. Ask the [corresponding code owner](./CODEOWNERS) to approve and merge the change. Note: you are allowed to approve your own MR.
4. Once the change is merged, the new release will be deployed during the next server synchronization. The server is synchronized once every 5 minutes at a random time.

If your change is not deployed after 10 minutes or you encounter any issue, please contact _Demurgos_ on Discord.

The server deployments are managed by a custom tool named [Katal](https://gitlab.com/eternaltwin/katal).
The deployment scripts are located in [this directory](https://gitlab.com/eternaltwin/katal/-/tree/master/crates/katal/src/etwin) and written in Rust.

## Configuration

Katal loads its configuration from this repository. Files are treated as JSON
values in nested JSON  objects corresponding to their location.

For example, the file `apps/neoparc/.json` with the content `{"repository": "https://gitlab.com/eternaltwin/dinoparc/dinoparc.git"}`
corresponds to the following JSON value:

```json
{"apps": {"neoparc":
  {
    "repository": "https://gitlab.com/eternaltwin/dinoparc/dinoparc.git"
  }
}}
```

The final config is then created by merging all the JSON objects, starting from
the most nested ones. JSON objects are merged recursively, other values (strings,
arrays, numbers, ...) are over-written.

## State

Your application state is what persists across deployments. For most apps it's
simply the database.

By default, the deployment script runs an "upgrade" step to ensure that the
state is compatible with the version of your app that you're deploying. More simply,
it runs your DB migration script.

You can change this default by setting the `state` option explicitly. The only
supported action currently is `Reset`: it deletes all previous data and starts
with a fresh database.

For example, set the following config in `./pousty/apps/<appName>/channels/<channelName>.json`.
```
{
  "release": "<some_commit_hash>",
  "state": {
    "action": "Reset",
    "token": "2022-10-15"
  }
}
```

The field `token` acts as an idempotency key: this ensures the reset is applied only once.
You can pick any string your want, however it is recommended by convention to use the
current date (and optionally time).

## Secrets

Secret values are defined in the `./pousty/vault/` directory. They follow the same resolution
rules as [the regular configuration values](#configuration), except there is an extra step
after the resolution to get the actual values.

The final step depends on the prefix:
- `generate:`: Generate a unique value, using the suffix, path, and a secret seed.
- `aead:`: Value encrypted with the master password.
  For example to set `emush.production.secret` to `foo`, use `cargo run -- vault encrypt` and then
  1. Provide the super key (master password)
  2. Enter `emush.production.secret` for the id
  3. Enter the secret `foo`
  4. Write the output in `./pousty/vault/emsuh/production.json`, `{"secret": "aead:<output>"}`
